export const getList = async(query) => {
    const headers = {
        Accept: 'application/vnd.github.v3+json'
    }
    const queryParams = Object.keys(query).length > 0 ? Object.keys(query).reduce((accum, key)=> {
        if(query[key]) return `${accum}${key}=${query[key]}&`
        else return accum
    }, '?') : ''
    const url = 'https://api.github.com/repos/facebook/react/issues'
    const response = await fetch(`${url}${queryParams}`, {headers})
    return response.json()
}