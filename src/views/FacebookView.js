import { Component } from "react";
import { connect } from "react-redux";
import Searcher from "../components/Searcher";
import { List, ListItem } from "../components/List";
import Breadcrumb from "../components/Breadcrumb";
import Issue from "../components/Issue";
import { fetchIssues } from "../store/actions/issues.actions";

class FacebookView extends Component {
  state = {
    creator: "",
  };
  componentDidMount() {
    this.fetchData({});
  }
  handleOnChangeSearcher(searchText) {
    if (!searchText) this.fetchData({});
    this.setState({ creator: searchText });
  }
  handleOnEnterSearcher() {
    const { creator } = this.state;
    if (creator.length) this.fetchData({ creator });
    else this.fetchData({});
  }
  fetchData({ creator, per_page = 10 }) {
    this.props.dispatch(fetchIssues({ creator, per_page }));
  }
  render() {
    const { creator } = this.state;
    return (
      <div>
        <Breadcrumb links={['facebook', 'react']}/>
        <Searcher
          value={creator}
          handleChange={this.handleOnChangeSearcher.bind(this)}
          handleEnter={this.handleOnEnterSearcher.bind(this)}
          placeholder="Search by creator e.g gaearon"
        />
        <List>
          {this.props.issues.list.map((issue) => {
            return (
              <ListItem key={issue.id}>
                <Issue {...issue} />
              </ListItem>
            );
          })}
        </List>
      </div>
    );
  }
}

function mapStateToProps({ issues }) {
  return {
    issues,
  };
}

export default connect(mapStateToProps)(FacebookView);
