import { FETCH_ISSUES } from '../actions/issues.actions.js'

export default function issues (state = { list: []}, action) {
  switch(action.type) {
    case FETCH_ISSUES :
      return {...state, list: action.issues}
    default :
      return state
  }
}