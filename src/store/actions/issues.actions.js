import { getList } from '../../utils/api'
export const FETCH_ISSUES = 'FETCH_ISSUES'



export const fetchIssues = (query = {}) => async dispatch => {
  const issues = await getList(query)
  dispatch({  
    type: FETCH_ISSUES,
    issues
  });
};