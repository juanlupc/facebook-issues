import { createStore } from 'redux'
import reducer from './reducers'
import middlewares from './middlewares'

export default createStore(reducer, middlewares)
