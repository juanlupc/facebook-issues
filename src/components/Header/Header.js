import "./Header.css";
export default function Header({title}) {
  return (
    <header>
        {title}
    </header>
  );
}
