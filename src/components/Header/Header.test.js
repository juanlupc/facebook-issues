import React from 'react';
import Header from './Header';
import { shallow } from 'enzyme';

const text = 'My Title'
describe('Header', () => {
  it('renders the title', () => {
    const wrapper = shallow(<Header title={text} />);
    expect(wrapper.text()).toEqual(text);
});
});

