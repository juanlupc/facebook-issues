import moment from 'moment';
import Tag from "../Tag";
import "./Issue.css";

export default function Issue({ title, labels, user, number, created_at }) {
  return (
    <div className="issue">
      <div className="issue-body">
        <p>{title}</p>
        {labels.map((label) => (
          <Tag
            key={label.id}
            {...label}
            color={`#${label.color.toUpperCase()}`}
          />
        ))}
      </div>
      <div className="issue-footer">
        <span>{`#${number} opened ${moment(created_at).fromNow()} by `}<b>{user.login}</b></span>
      </div>
    </div>
  );
}
