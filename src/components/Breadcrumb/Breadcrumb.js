import "./Breadcrumb.css";

export default function Breadcrumb({ links }) {
  return (
    <h1 className="breadcrumb">
      {links.map((link, index) => {
        if (index === links.length - 1) return <a href="/" key={link}><strong>{link}</strong></a>;
        else
          return (
            <a key={link} href="/">
              {link}
              <span>/</span>
            </a>
          );
      })}
    </h1>
  );
}
