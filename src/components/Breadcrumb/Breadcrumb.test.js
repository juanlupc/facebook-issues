import React from 'react';
import Breadcrumb from './Breadcrumb';
import { shallow } from 'enzyme';

const links = ['hello', 'work'];
describe('Breadcrumb', () => {
  it('renders the links', () => {
    const wrapper = shallow(<Breadcrumb links={links} />);
    expect(wrapper.find('a')).toHaveLength(2);

});
});

