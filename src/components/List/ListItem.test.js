import React from 'react';
import ListItem from './ListItem';
import { shallow } from 'enzyme';

describe('ListItem', () => {
  it('renders the children', () => {
    const wrapper = shallow(<ListItem><p>Hello</p></ListItem>);
    expect(wrapper.html()).toEqual('<li><p>Hello</p></li>');
});
});