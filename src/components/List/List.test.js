import React from 'react';
import List from './List';
import { shallow } from 'enzyme';

describe('List', () => {
  it('renders the children', () => {
    const wrapper = shallow(<List><p>Hello</p></List>);
    expect(wrapper.html()).toEqual('<ul><p>Hello</p></ul>');
});
});

