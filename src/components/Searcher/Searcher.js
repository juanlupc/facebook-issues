import './Searcher.css'

export default function Searcher({handleEnter, handleChange,value = '', placeholder}) {
    const handleKeyDown = (e) => {
        if (e.key === 'Enter') {
            handleEnter(e)
        }
    }
    const handleOnChange = (e) => {
        e.preventDefault()
        handleChange(e.target.value)
    }
    return (
        <div className="searcher">
            <input type="text" value={value} placeholder={placeholder} onChange={handleOnChange} onKeyDown={handleKeyDown}/>
        </div>
    )}
   