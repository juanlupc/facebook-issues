import React from 'react';
import Searcher from './Searcher';
import { shallow } from 'enzyme';

describe('Searcher', () => {
    it('should call onChange prop with input value', () => {
        const onChange = jest.fn();
        const event = {
            target: { value: 'custom value' },
            preventDefault: ()=>{}
          }
        const component = shallow(<Searcher handleChange={onChange} value="custom value" />);
        component.find('input').simulate('change', event);
        expect(onChange).toBeCalledWith('custom value');
      });
});

