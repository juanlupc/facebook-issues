import './Tag.css'
export default function Tag ({color, name}) {
    return (<span className='tag' style={{background: color}}>{name}</span>)
}