import React from 'react';
import Tag from './Tag';
import { shallow } from 'enzyme';

const tag = { name: 'Tag', color: 'red'}
describe('Tag', () => {
  it('renders color and name', () => {
    const wrapper = shallow(<Tag {...tag} />);
    expect(wrapper.text()).toEqual(tag.name);
    expect(wrapper.prop('style')).toHaveProperty('background', tag.color);

});
});