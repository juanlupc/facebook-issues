import Header from './components/Header'
import FacebookView from './views/FacebookView'

function App() {
  return (
    <div className="App">
        <Header title="GitHub" />
    <main>
        <FacebookView/>
    </main>
    </div>
  );
}

export default App;
