# About The Project

This project is a little demo of the website [Issues of React in Github](https://github.com/facebook/react/issues).

# Getting Started

## Prerequisites

- [Git](https://git-scm.com/)
- [Node.js](nodejs.org)
- [NPM](https://www.npmjs.com/) or [YARN](https://yarnpkg.com/)

## Installation

1. Clone the repo

```git clone https://juanlupc@bitbucket.org/juanlupc/facebook-issues.git```

2. Install NPM packages

```yarn install```
    or 
```npm install```

3. Run the app

```yarn start```
    or 
```npm run start```

4. Run the test

```yarn test```
    or 
```npm run test```


# Usage & Development

As the task development time is limited, I would like to explain some aspects:
1. In the first analysis, I have considered that the most important parts on the page are the list of issues and the searcher.
2. To get the errors, I have used the Github API. The [GET method](https://docs.github.com/en/rest/reference/issues#list-repository-issues) does not search on the title of the issues. The searcher only searches for the creator (user that created the issues).
3. I have divided the page into simple components (reusability).
4.  I have used Redux to manage the state of the application.
5. For the tests I have used Jest and Enzyme.